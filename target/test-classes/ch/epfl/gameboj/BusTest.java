package ch.epfl.gameboj;

import ch.epfl.gameboj.component.Component;
import java.util.List;
import java.util.Collections;
import java.util.Arrays;
import java.util.Random;
import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class BusTest
{
    private static SimpleComponent[] newComponents(final int n) {
        final SimpleComponent[] cs = new SimpleComponent[n];
        for (int i = 0; i < cs.length; ++i) {
            cs[i] = new SimpleComponent(i, i);
        }
        return cs;
    }
    
    @Test
    void attachFailsForNullComponent() {
        final Bus b = new Bus();
        Assertions.assertThrows((Class)NullPointerException.class, () -> b.attach((Component)null));
    }
    
    @Test
    void readFailsForInvalidAddress() {
        final Bus b = new Bus();
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            int a0;
            for (a0 = rng.nextInt(); a0 >= 0 && a0 <= 65535; a0 += 65535) {}
            final int a2 = a0;
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> b.read(a2));
        }
    }
    
    @Test
    void readReturnsCorrectValue() {
        final Component[] cs = (Component[])newComponents(20);
        Collections.shuffle(Arrays.asList(cs), TestRandomizer.newRandom());
        final Bus b = new Bus();
        Component[] array;
        for (int length = (array = cs).length, j = 0; j < length; ++j) {
            final Component c = array[j];
            b.attach(c);
        }
        for (int i = 0; i < cs.length; ++i) {
            Assertions.assertEquals(i, b.read(i));
        }
    }
    
    @Test
    void readReturnsCorrectDefaultValue() {
        final Random rng = TestRandomizer.newRandom();
        final Bus b = new Bus();
        for (int i = 0; i < 100; ++i) {
            final int a = rng.nextInt(65536);
            Assertions.assertEquals(255, b.read(a));
        }
    }
    
    @Test
    void writeWritesToAllComponents() {
        final SimpleComponent[] cs = newComponents(20);
        final Bus b = new Bus();
        SimpleComponent[] array;
        for (int length = (array = cs).length, i = 0; i < length; ++i) {
            final Component c = (Component)array[i];
            b.attach(c);
        }
        b.write(0, 42);
        SimpleComponent[] array2;
        for (int length2 = (array2 = cs).length, j = 0; j < length2; ++j) {
            final SimpleComponent c2 = array2[j];
            Assertions.assertTrue(c2.wasWritten());
        }
    }
    
    @Test
    void writeWritesCorrectValue() {
        final SimpleComponent[] cs = newComponents(20);
        final Bus b = new Bus();
        SimpleComponent[] array;
        for (int length = (array = cs).length, j = 0; j < length; ++j) {
            final Component c = (Component)array[j];
            b.attach(c);
        }
        for (int i = 0; i < cs.length; ++i) {
            b.write(i, i * 2018 & 0xFF);
        }
        for (int i = 0; i < cs.length; ++i) {
            Assertions.assertEquals(i * 2018 & 0xFF, b.read(i));
        }
    }
    
    @Test
    void writeFailsForInvalidAddress() {
        final Random rng = TestRandomizer.newRandom();
        final Bus b = new Bus();
        for (int i = 0; i < 100; ++i) {
            int a0;
            for (a0 = rng.nextInt(); a0 >= 0 && a0 <= 65535; a0 += 65535) {}
            final int a2 = a0;
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> b.write(a2, 0));
        }
    }
    
    @Test
    void writeFailsForInvalidData() {
        final Random rng = TestRandomizer.newRandom();
        final Bus b = new Bus();
        for (int i = 0; i < 100; ++i) {
            int d0;
            for (d0 = rng.nextInt(); d0 >= 0 && d0 <= 255; d0 += 255) {}
            final int d2 = d0;
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> b.write(0, d2));
        }
    }
}