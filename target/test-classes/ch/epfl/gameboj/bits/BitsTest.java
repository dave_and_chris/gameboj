package ch.epfl.gameboj.bits;

import java.util.Random;
import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

class BitsTest
{
    private static int[] ALL_MASKS;
    
    static {
        BitsTest.ALL_MASKS = new int[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, Integer.MIN_VALUE };
    }
    
    @Test
    void maskFailsForInvalidIndex() {
        Assertions.assertThrows((Class)IndexOutOfBoundsException.class, () -> Bits.mask(-1));
        Assertions.assertThrows((Class)IndexOutOfBoundsException.class, () -> Bits.mask(32));
    }
    
    @Test
    void maskWorksBetween0And31() {
        for (int i = 0; i < 32; ++i) {
            Assertions.assertEquals(BitsTest.ALL_MASKS[i], Bits.mask(i));
        }
    }
    
    @Test
    void testFailsForInvalidIndex() {
        Assertions.assertThrows((Class)IndexOutOfBoundsException.class, () -> Bits.test(0, -1));
        Assertions.assertThrows((Class)IndexOutOfBoundsException.class, () -> Bits.test(0, 32));
    }
    
    @Test
    void testWorksForValuesWithSingleBitSet() {
        for (int i = 0; i < 32; ++i) {
            final int m = BitsTest.ALL_MASKS[i];
            for (int j = 0; j < 32; ++j) {
                Assertions.assertEquals((Object)(i == j), (Object)Bits.test(m, j));
            }
        }
    }
    
    @Test
    void test2WorksForValuesWithSingleBitSet() {
        for (int i = 0; i < 32; ++i) {
            final int m = BitsTest.ALL_MASKS[i];
            BitsTest.AllBits[] values;
            for (int length = (values = BitsTest.AllBits.values()).length, j = 0; j < length; ++j) {
                final Bit b = (Bit)values[j];
                Assertions.assertEquals((Object)(i == b.ordinal()), (Object)Bits.test(m, b));
            }
        }
    }
    
    @Test
    void setFailsForInvalidIndex() {
        Assertions.assertThrows((Class)IndexOutOfBoundsException.class, () -> Bits.set(0, -1, true));
        Assertions.assertThrows((Class)IndexOutOfBoundsException.class, () -> Bits.set(0, 32, true));
    }
    
    @Test
    void setDoesNotChangeValueWhenItDoesNotChangeBit() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = rng.nextInt();
            final int j = rng.nextInt(32);
            Assertions.assertEquals(v, Bits.set(v, j, Bits.test(v, j)));
        }
    }
    
    @Test
    void setCanSetAndClearAllIndividualBits() {
        for (int i = 0; i < 32; ++i) {
            final int m = BitsTest.ALL_MASKS[i];
            Assertions.assertEquals(m, Bits.set(0, i, true));
            Assertions.assertEquals(0, Bits.set(m, i, false));
        }
    }
    
    @Test
    void clipFailsForInvalidSize() {
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.clip(-1, 0));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.clip(33, 0));
    }
    
    @Test
    void clipWorksForMaximumSize() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = rng.nextInt();
            Assertions.assertEquals(v, Bits.clip(32, v));
            Assertions.assertEquals(~v, Bits.clip(32, ~v));
        }
    }
    
    @Test
    void clipWorksForNonMaximumSizes() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 32; ++i) {
            for (int j = 0; j < 100; ++j) {
                final int b = Bits.clip(i, rng.nextInt());
                Assertions.assertTrue(b >= 0 && b <= BitsTest.ALL_MASKS[i] - 1);
            }
        }
    }
    
    @Test
    void extractFailsForInvalidRanges() {
        final int[][] invalidRanges = { { -1, 1 }, { 0, 33 }, { 1, 32 } };
        int[][] array;
        for (int length = (array = invalidRanges).length, i = 0; i < length; ++i) {
            final int[] r = array[i];
            Assertions.assertThrows((Class)IndexOutOfBoundsException.class, () -> Bits.extract(0, r[0], r[1]));
        }
    }
    
    @Test
    void extractAndTestAreCompatible() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = rng.nextInt();
            final int b = rng.nextInt(32);
            final int e = Bits.test(v, b) ? 1 : 0;
            Assertions.assertEquals(e, Bits.extract(v, b, 1));
        }
    }
    
    @Test
    void extractCanExtractEverything() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = rng.nextInt();
            Assertions.assertEquals(v, Bits.extract(v, 0, 32));
        }
    }
    
    @Test
    void extractCanExtractNothing() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = rng.nextInt();
            final int b = rng.nextInt(32);
            Assertions.assertEquals(0, Bits.extract(v, b, 0));
        }
    }
    
    @Test
    void extractWorksOnKnownValues() {
        Assertions.assertEquals(15, Bits.extract(-19088744, 28, 4));
        Assertions.assertEquals(14, Bits.extract(-19088744, 24, 4));
        Assertions.assertEquals(13, Bits.extract(-19088744, 20, 4));
        Assertions.assertEquals(12, Bits.extract(-19088744, 16, 4));
        Assertions.assertEquals(11, Bits.extract(-19088744, 12, 4));
        Assertions.assertEquals(10, Bits.extract(-19088744, 8, 4));
        Assertions.assertEquals(9, Bits.extract(-19088744, 4, 4));
        Assertions.assertEquals(8, Bits.extract(-19088744, 0, 4));
    }
    
    @Test
    void rotateFailsForInvalidSize() {
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.rotate(0, 0, 0));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.rotate(33, 0, 0));
    }
    
    @Test
    void rotateWorksOnFullInt() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = rng.nextInt();
            final int d = rng.nextInt();
            Assertions.assertEquals(Integer.rotateLeft(v, d), Bits.rotate(32, v, d));
            Assertions.assertEquals(Integer.rotateLeft(~v, d), Bits.rotate(32, ~v, d));
        }
    }
    
    @Test
    void rotateDoesNothingWhenAllBitsAreSet() {
        final Random rng = TestRandomizer.newRandom();
        for (int s = 1; s < 32; ++s) {
            final int v = BitsTest.ALL_MASKS[s] - 1;
            for (int i = 0; i < 100; ++i) {
                final int d = rng.nextInt();
                Assertions.assertEquals(v, Bits.rotate(s, v, d));
            }
        }
    }
    
    @Test
    void rotateDoesNothingWhenDistanceIsAMultipleOfSize() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v8 = rng.nextInt(256);
            final int k = rng.nextInt(11) - 5;
            Assertions.assertEquals(v8, Bits.rotate(8, v8, k * 8));
        }
    }
    
    @Test
    void rotateIsPeriodic() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v24 = rng.nextInt(16777216);
            final int d = rng.nextInt(24);
            final int k = rng.nextInt(11) - 5;
            Assertions.assertEquals(Bits.rotate(24, v24, d), Bits.rotate(24, v24, d + 24 * k));
        }
    }
    
    @Test
    void rotateWorksOnKnownValues() {
        Assertions.assertEquals(1, Bits.rotate(1, 1, 1));
        Assertions.assertEquals(1, Bits.rotate(1, 1, -1));
        Assertions.assertEquals(2, Bits.rotate(2, 1, 1));
        Assertions.assertEquals(2, Bits.rotate(2, 1, -1));
        Assertions.assertEquals(2, Bits.rotate(3, 1, 1));
        Assertions.assertEquals(4, Bits.rotate(3, 1, 2));
        Assertions.assertEquals(4, Bits.rotate(3, 1, -1));
        Assertions.assertEquals(10, Bits.rotate(4, 5, 1));
        Assertions.assertEquals(10, Bits.rotate(4, 5, -3));
        Assertions.assertEquals(5, Bits.rotate(4, 5, 2));
        Assertions.assertEquals(5, Bits.rotate(4, 5, -2));
        Assertions.assertEquals(10, Bits.rotate(4, 5, 3));
        Assertions.assertEquals(26, Bits.rotate(5, 13, 1));
        Assertions.assertEquals(21, Bits.rotate(5, 13, 2));
        Assertions.assertEquals(11, Bits.rotate(5, 13, 3));
        Assertions.assertEquals(22, Bits.rotate(5, 13, 4));
    }
    
    @Test
    void signExtend8FailsOnInvalidValue() {
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.signExtend8(256));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.signExtend8(-1));
    }
    
    @Test
    void signExtend8WorksOnAllValidValues() {
        for (int i = -128; i <= 127; ++i) {
            Assertions.assertEquals(i, Bits.signExtend8(i & 0xFF));
        }
    }
    
    @Test
    void reverse8FailsOnInvalidValue() {
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.reverse8(256));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.reverse8(-1));
    }
    
    @Test
    void reverse8IsItsOwnInverse() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v8 = rng.nextInt(256);
            Assertions.assertEquals(v8, Bits.reverse8(Bits.reverse8(v8)));
        }
    }
    
    @Test
    void reverse8WorksOnKnownValues() {
        Assertions.assertEquals(0, Bits.reverse8(0));
        Assertions.assertEquals(255, Bits.reverse8(255));
        Assertions.assertEquals(72, Bits.reverse8(18));
        Assertions.assertEquals(53, Bits.reverse8(172));
    }
    
    @Test
    void complement8FailsOnInvalidValue() {
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.complement8(256));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.complement8(-1));
    }
    
    @Test
    void complement8IsItsOwnInverse() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v8 = rng.nextInt(256);
            Assertions.assertEquals(v8, Bits.complement8(Bits.complement8(v8)));
        }
    }
    
    @Test
    void complement8WorksOnKnownValues() {
        Assertions.assertEquals(255, Bits.complement8(0));
        Assertions.assertEquals(170, Bits.complement8(85));
        Assertions.assertEquals(85, Bits.complement8(170));
        Assertions.assertEquals(198, Bits.complement8(57));
    }
    
    @Test
    void make16FailsOnInvalidValues() {
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.make16(256, 0));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.make16(-1, 0));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.make16(0, 256));
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Bits.make16(0, -1));
    }
    
    @Test
    void make16WorksOnKnownValues() {
        Assertions.assertEquals(43605, Bits.make16(170, 85));
        Assertions.assertEquals(0, Bits.make16(0, 0));
        Assertions.assertEquals(21930, Bits.make16(85, 170));
        Assertions.assertEquals(4660, Bits.make16(18, 52));
    }
    
    @Test
    void make16AndExtractAreCompatible() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int h = rng.nextInt(256);
            final int l = rng.nextInt(256);
            final int v = Bits.make16(h, l);
            Assertions.assertEquals(h, Bits.extract(v, 8, 8));
            Assertions.assertEquals(l, Bits.extract(v, 0, 8));
        }
    }
}