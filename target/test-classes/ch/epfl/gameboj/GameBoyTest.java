package ch.epfl.gameboj;

import org.junit.jupiter.api.Test;
import javax.sound.sampled.LineUnavailableException;
import org.junit.jupiter.api.Assertions;
import ch.epfl.gameboj.component.cartridge.Cartridge;

public class GameBoyTest
{
    @Test
    void workRamIsProperlyMapped() throws InterruptedException, LineUnavailableException {
        final Bus b = new GameBoy((Cartridge)null).getBus();
        for (int a = 0; a <= 65535; ++a) {
            final boolean inWorkRamOrEcho = 49152 <= a && a < 65024;
            Assertions.assertEquals(inWorkRamOrEcho ? 0 : 255, b.read(a), String.format("at address 0x%04x", a));
        }
    }
    
    @Test
    void workRamCanBeReadAndWritten() throws InterruptedException, LineUnavailableException {
        final Bus b = new GameBoy((Cartridge)null).getBus();
        for (int a = 49152; a < 57344; ++a) {
            b.write(a, (a ^ 0xA5) & 0xFF);
        }
        for (int a = 49152; a < 57344; ++a) {
            Assertions.assertEquals((a ^ 0xA5) & 0xFF, b.read(a));
        }
    }
    
    @Test
    void echoAreaReflectsWorkRam() throws InterruptedException, LineUnavailableException {
        final Bus b = new GameBoy((Cartridge)null).getBus();
        for (int a = 49152; a < 57344; ++a) {
            b.write(a, (a ^ 0xA5) & 0xFF);
        }
        for (int a = 57344; a < 65024; ++a) {
            Assertions.assertEquals((a - 8192 ^ 0xA5) & 0xFF, b.read(a));
        }
        for (int a = 57344; a < 65024; ++a) {
            b.write(a, (a ^ 0xA5) & 0xFF);
        }
        for (int a = 49152; a < 56832; ++a) {
            Assertions.assertEquals((a + 8192 ^ 0xA5) & 0xFF, b.read(a));
        }
    }
}