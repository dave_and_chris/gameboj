package ch.epfl.gameboj.component;

import org.hamcrest.MatcherAssert;
import org.hamcrest.CoreMatchers;
import ch.epfl.gameboj.bits.Bits;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.junit.jupiter.api.BeforeAll;
import ch.epfl.gameboj.component.cpu.Cpu;

class JoypadTest
{
    static Cpu mockCpu;
    Joypad joypad;
    
    @BeforeAll
    static void setupBeforeAll() {
    }
    
    @BeforeEach
    void setupBeforeEach() {
        JoypadTest.mockCpu = (Cpu)Mockito.mock((Class)Cpu.class);
        this.joypad = new Joypad(JoypadTest.mockCpu);
    }
    
    @Test
    void testJoypadConstructorRefusesNullCpu() {
        Assertions.assertThrows((Class)NullPointerException.class, () -> new Joypad((Cpu)null));
    }
    
    @Test
    void testKeyPressed() {
        this.joypad.write(65280, Bits.complement8(32));
        this.joypad.keyPressed(Joypad.Key.A);
        MatcherAssert.assertThat((Object)this.joypad.read(65280), CoreMatchers.is(CoreMatchers.equalTo((Object)Bits.complement8(33))));
    }
    
    @Test
    void testKeyReleased() {
        this.joypad.write(65280, Bits.complement8(32));
        this.joypad.keyPressed(Joypad.Key.A);
        this.joypad.keyReleased(Joypad.Key.A);
        MatcherAssert.assertThat((Object)this.joypad.read(65280), CoreMatchers.is(CoreMatchers.equalTo((Object)Bits.complement8(32))));
    }
    
    @Test
    void testP1requestsInterrupt() {
        this.joypad.write(65280, Bits.complement8(32));
        this.joypad.keyPressed(Joypad.Key.A);
        ((Cpu)Mockito.verify((Object)JoypadTest.mockCpu)).requestInterrupt(Cpu.Interrupt.JOYPAD);
    }
    
    @Test
    void testReadReturnsNO_DATAWhenOtherAddress() {
        this.joypad.write(65280, Bits.complement8(170));
        MatcherAssert.assertThat((Object)this.joypad.read(65260), CoreMatchers.is(CoreMatchers.equalTo((Object)256)));
    }
    
    @Test
    void testReadReturnsCorrectDataWhenRegAddress() {
        this.joypad.write(65280, Bits.complement8(48));
        MatcherAssert.assertThat((Object)this.joypad.read(65280), CoreMatchers.is(CoreMatchers.equalTo((Object)Bits.complement8(48))));
    }
    
    @Test
    void testWriteDoesNothingWhenOtherAddress() {
        this.joypad.write(65260, Bits.complement8(240));
        MatcherAssert.assertThat((Object)this.joypad.read(65280), CoreMatchers.is(CoreMatchers.equalTo((Object)Bits.complement8(0))));
    }
    
    @Test
    void testWriteDoesNotModifyP1LSB() {
        this.joypad.write(65280, Bits.complement8(15));
        MatcherAssert.assertThat((Object)this.joypad.read(65280), CoreMatchers.is(CoreMatchers.equalTo((Object)Bits.complement8(0))));
    }
    
    @Test
    void testWriteWritesWhenRegAddress() {
        this.joypad.write(65280, Bits.complement8(32));
        MatcherAssert.assertThat((Object)this.joypad.read(65280), CoreMatchers.is(CoreMatchers.equalTo((Object)Bits.complement8(32))));
    }
}