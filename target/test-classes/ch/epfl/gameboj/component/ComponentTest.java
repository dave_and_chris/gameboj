package ch.epfl.gameboj.component;

import org.junit.jupiter.api.Test;
import java.util.Random;
import org.junit.jupiter.api.Assertions;
import ch.epfl.test.TestRandomizer;

public interface ComponentTest
{
    Component newComponent();
    
    @Test
    default void readFailsForInvalidAddress() {
        final Random rng = TestRandomizer.newRandom();
        final Component c = this.newComponent();
        for (int i = 0; i < 100; ++i) {
            int a0;
            for (a0 = rng.nextInt(); a0 >= 0 && a0 <= 65535; a0 += 65535) {}
            final int a2 = a0;
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> c.read(a2));
        }
    }
    
    @Test
    default void writeFailsForInvalidAddress() {
        final Random rng = TestRandomizer.newRandom();
        final Component c = this.newComponent();
        for (int i = 0; i < 100; ++i) {
            int a0;
            for (a0 = rng.nextInt(); a0 >= 0 && a0 <= 65535; a0 += 65535) {}
            final int a2 = a0;
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> c.write(a2, 0));
        }
    }
    
    @Test
    default void writeFailsForInvalidData() {
        final Random rng = TestRandomizer.newRandom();
        final Component c = this.newComponent();
        for (int i = 0; i < 100; ++i) {
            int d0;
            for (d0 = rng.nextInt(); d0 >= 0 && d0 <= 255; d0 += 255) {}
            final int d2 = d0;
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> c.write(0, d2));
        }
    }
}