package ch.epfl.gameboj.component;

import java.util.Random;
import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import ch.epfl.gameboj.component.cpu.Cpu;
import ch.epfl.gameboj.component.time.Timer;

public final class TimerTest implements ComponentTest
{
    public Timer newComponent() {
        return new Timer(new Cpu());
    }
    
    @Test
    void constructorFailsWhenCpuIsNull() {
        Assertions.assertThrows((Class)NullPointerException.class, () -> new Timer((Cpu)null));
    }
    
    @Test
    void timerRegistersAreInitially0() {
        final Timer t = this.newComponent();
        Assertions.assertEquals(0, t.read(65284));
        Assertions.assertEquals(0, t.read(65285));
        Assertions.assertEquals(0, t.read(65286));
        Assertions.assertEquals(0, t.read(65287));
    }
    
    @Test
    void timaCanBeWrittenAndRead() {
        final Timer t = this.newComponent();
        for (int tima = 0; tima <= 255; ++tima) {
            t.write(65285, tima);
            Assertions.assertEquals(tima, t.read(65285));
        }
    }
    
    @Test
    void tmaCanBeWrittenAndRead() {
        final Timer t = this.newComponent();
        for (int tma = 0; tma <= 255; ++tma) {
            t.write(65286, tma);
            Assertions.assertEquals(tma, t.read(65286));
        }
    }
    
    @Test
    void tacCanBeWrittenAndRead() {
        final Timer t = this.newComponent();
        for (int tac = 0; tac <= 7; ++tac) {
            t.write(65287, tac);
            Assertions.assertEquals(tac, t.read(65287));
        }
    }
    
    @Test
    void cycleProperlyIncrementsMainCounter() {
        final Timer t = this.newComponent();
        for (int c = 0; c <= 65535; ++c) {
            Assertions.assertEquals(c >> 6 & 0xFF, t.read(65284));
            t.cycle((long)c);
        }
    }
    
    @Test
    void mainCounterGetsResetByAnyWriteToDIV() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final Timer t = this.newComponent();
            t.write(65284, 0);
            for (int c = 0; c <= 63; ++c) {
                t.cycle((long)c);
            }
            Assertions.assertEquals(1, t.read(65284));
            t.write(65284, rng.nextInt(256));
            Assertions.assertEquals(0, t.read(65284));
        }
    }
    
    @Test
    void secondaryCounterDoesNotChangeWhenDisabled() {
        final Timer t = this.newComponent();
        t.write(65287, 0);
        for (int c = 0; c < 2027; ++c) {
            Assertions.assertEquals(0, t.read(65285));
            t.cycle((long)c);
        }
    }
    
    @Test
    void secondaryCounterIncrementsProperlyWhenTacIs0() {
        final Timer t = this.newComponent();
        t.write(65287, 4);
        for (int c = 0; c < 2027; ++c) {
            Assertions.assertEquals(c >> 8 & 0xFF, t.read(65285));
            t.cycle((long)c);
        }
    }
    
    @Test
    void secondaryCounterIncrementsProperlyWhenTacIs1() {
        final Timer t = this.newComponent();
        t.write(65287, 5);
        for (int c = 0; c < 2027; ++c) {
            Assertions.assertEquals(c >> 2 & 0xFF, t.read(65285));
            t.cycle((long)c);
        }
    }
    
    @Test
    void secondaryCounterIncrementsProperlyWhenTacIs2() {
        final Timer t = this.newComponent();
        t.write(65287, 6);
        for (int c = 0; c < 2027; ++c) {
            Assertions.assertEquals(c >> 4 & 0xFF, t.read(65285));
            t.cycle((long)c);
        }
    }
    
    @Test
    void secondaryCounterIncrementsProperlyWhenTacIs3() {
        final Timer t = this.newComponent();
        t.write(65287, 7);
        for (int c = 0; c < 2027; ++c) {
            Assertions.assertEquals(c >> 6 & 0xFF, t.read(65285));
            t.cycle((long)c);
        }
    }
    
    @Test
    void secondaryCounterIncrementsWhenGettingDisabledAndStateIs1() {
        final Timer t = this.newComponent();
        t.write(65287, 5);
        t.cycle(0L);
        t.cycle(1L);
        Assertions.assertEquals(0, t.read(65285));
        t.write(65287, 0);
        Assertions.assertEquals(1, t.read(65285));
    }
    
    @Test
    void secondaryCounterGetsResetToTMAOnOverflow() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int tma = rng.nextInt(256);
            final Timer t = this.newComponent();
            t.write(65287, 5);
            t.write(65286, tma);
            for (int c = 0; c <= 1024; ++c) {
                t.cycle((long)c);
            }
            Assertions.assertEquals(t.read(65285), tma);
        }
    }
}