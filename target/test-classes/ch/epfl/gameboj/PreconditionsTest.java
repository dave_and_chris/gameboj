package ch.epfl.gameboj;

import java.util.Random;
import ch.epfl.test.TestRandomizer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PreconditionsTest
{
    @Test
    void checkArgumentSucceedsForTrue() {
        Preconditions.checkArgument(true);
    }
    
    @Test
    void checkArgumentFailsForFalse() {
        Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Preconditions.checkArgument(false));
    }
    
    @Test
    void checkBits8SucceedsFor8BitValues() {
        for (int i = 0; i <= 255; ++i) {
            Preconditions.checkBits8(i);
        }
    }
    
    @Test
    void chechBits8FailsForNegativeValues() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = -rng.nextInt(Integer.MAX_VALUE);
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Preconditions.checkBits8(v));
        }
    }
    
    @Test
    void checkBits8FailsForTooBigValues() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = 256 + rng.nextInt(1000);
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Preconditions.checkBits8(v));
        }
    }
    
    @Test
    void checkBits16SucceedsFor16BitValues() {
        for (int i = 0; i <= 65535; ++i) {
            Preconditions.checkBits16(i);
        }
    }
    
    @Test
    void chechBits16FailsForNegativeValues() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = -rng.nextInt(Integer.MAX_VALUE);
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Preconditions.checkBits16(v));
        }
    }
    
    @Test
    void checkBits16FailsForTooBigValues() {
        final Random rng = TestRandomizer.newRandom();
        for (int i = 0; i < 100; ++i) {
            final int v = 65536 + rng.nextInt(1000);
            Assertions.assertThrows((Class)IllegalArgumentException.class, () -> Preconditions.checkBits16(v));
        }
    }
}